﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab1.Models
{
    public class RemovableRoom
    {
        public int Id { get; set; }
        public int IdRoom { get; set; }
        public int IdCustomer { get; set; }
        public string SettlementDate { get; set; }
        public string DateOfDeparture { get; set; }
    }
}
