﻿using System;
using System.Collections.Generic;
using System.Text;
namespace lab1.Models
{
    public class HotelRoom
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public int Coast { get; set; }
        public int Capacity { get; set; }
        public string Description { get; set; }
    }
}