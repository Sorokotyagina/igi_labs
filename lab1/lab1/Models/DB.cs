﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using lab1.Models;


namespace lab1.Models
{
    public static class DB
    {
        public static void Initialize(HotelRoomContext db)
        {
            db.Database.EnsureCreated();
            if (db.HotelRooms.Any() || db.Removables.Any())
            {
                return;
            }
            HotelRoom room1 = new HotelRoom() { Type = "Single",Coast = 100, Capacity = 1, Description = "Однокомнатный номер для одного человека с одной кроватью.Небольшой размер номера и простая обстановка" };
            HotelRoom room2 = new HotelRoom() { Type = "Double room", Coast = 150,Capacity = 2, Description = "Однокомнатный номер для двух человек с одной двуспальной кроватью." };
            HotelRoom room3 = new HotelRoom() { Type = "Twin", Coast = 200,Capacity = 2, Description = "Однокомнатный номер для трех человек с одной двуспальной кроватью и одной односпальной  или с тремя отдельными односпальными кроватями" };
            HotelRoom room4 = new HotelRoom() { Type = "Triple", Coast = 300, Capacity = 3, Description = "Однокомнатный номер для четырех человек с одной двуспальной кроватью и двумя односпальными или с четыремя отдельными односпальными кроватями." };

            db.HotelRooms.AddRange(new HotelRoom[] { room1, room2, room3, room4});
            db.SaveChanges();

            Customer customer1 = new Customer() { FIO = "Соловьев Ф.О", Passport = "HB294736", IdRoom = 1 };
            Customer customer2 = new Customer() { FIO = "Афанасьев А.И", Passport = "HB673432", IdRoom = 2 };
            Customer customer3 = new Customer() { FIO = "Ковалева С.М", Passport = "HB763123", IdRoom = 3 };
            Customer customer4 = new Customer() { FIO = "Михалек В.П", Passport = "H7832231", IdRoom = 4 };

            db.Customers.AddRange(new Customer[] { customer1, customer2, customer3, customer4 });
            db.SaveChanges();

            RemovableRoom roomRemovable1 = new RemovableRoom() { IdRoom = 1, IdCustomer = 1, SettlementDate = "10.01.2019", DateOfDeparture="20.01.2019"};
            RemovableRoom roomRemovable2 = new RemovableRoom() { IdRoom = 2, IdCustomer = 2, SettlementDate = "13.01.2019", DateOfDeparture = "17.01.2019" };
            RemovableRoom roomRemovable3 = new RemovableRoom() { IdRoom = 3, IdCustomer = 3, SettlementDate = "15.01.2019", DateOfDeparture = "30.01.2019" };
            RemovableRoom roomRemovable4 = new RemovableRoom() { IdRoom = 4, IdCustomer = 4, SettlementDate = "11.01.2019", DateOfDeparture = "13.01.2019" };

            db.Removables.AddRange(new RemovableRoom[] { roomRemovable1, roomRemovable2, roomRemovable3, roomRemovable4 });
            db.SaveChanges();

        }
    }
}
