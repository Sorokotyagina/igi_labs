﻿using System;
using System.Linq;
using System.Collections.Generic;
using lab1.Models;
using static System.Console;

namespace lab1
{
    class Program
    {
        static HotelRoomContext db = new HotelRoomContext();
        static void Main(string[] args)
        {
            DB.Initialize(db);
            WriteLine("1.	Выборку всех данных из таблицы, стоящей в схеме базы данных нас стороне отношения «один»\n");
            WriteLine("Номера: ");
            var rooms = db.HotelRooms.ToList();
            foreach (HotelRoom room in rooms)
                WriteLine($"Вид: {room.Type} Стоимость:{room.Coast} Вместимость: {room.Capacity}  Описание: {room.Description}");

            WriteLine("\n2.	Выборку данных из таблицы, стоящей в схеме базы данных нас стороне отношения «один», отфильтрованные по определенному условию, налагающему ограничения на одно или несколько полей\n");
            WriteLine("Номера ,отфильтрованные по количеству вместимости '2'");
            rooms = db.HotelRooms.Where(p => p.Capacity == 2).ToList();
            foreach (HotelRoom room in rooms)
                WriteLine($"Тип: {room.Type} Вместимость: {room.Capacity} Описание: {room.Description}");

            WriteLine("\n3.	Выборку данных, сгруппированных по любому из полей данных с выводом какого-либо итогового результата (min, max, avg, сount или др.) по выбранному полю из таблицы, стоящей в схеме базы данных нас стороне отношения «многие»\n");
            Write("кол-во номеров стоимостью 150р : ");
            WriteLine(db.HotelRooms.Where(p => p.Coast == 150).Count());

            WriteLine("\n4.	Выборку данных из двух полей двух таблиц, связанных между собой отношением «один-ко-многим» \n");
            WriteLine("Информация о снятых номерах: ");
            var listInf = db.Removables.Join(db.Customers, p => p.IdCustomer, c => c.Id, (p, c) => new { Name=c.FIO, Pass =c.Passport, SetDate = p.SettlementDate, DepDate = p.DateOfDeparture });
            foreach (var item in listInf)
                WriteLine($"ФИО: {item.Name} Паспорт: {item.Pass} Дата заезда: {item.SetDate} Дата выезда: {item.DepDate}");

            WriteLine("\n5.	Выборку данных из двух таблиц, связанных между собой отношением «один-ко-многим» и отфильтрованным по некоторому условию, налагающему ограничения на значения одного или нескольких полей \n");
            WriteLine("Информация о снятых номерах за 15.01.2019:");
            listInf = db.Removables.Join(db.Customers, p => p.IdCustomer, c => c.Id, (p, c) => new { Name = c.FIO, Pass = c.Passport, SetDate = p.SettlementDate, DepDate = p.DateOfDeparture }).Where(c => c.SetDate == "15.01.2019");
            foreach (var item in listInf)
                WriteLine($"ФИО: {item.Name} Паспорт: {item.Pass} Дата заезда: {item.SetDate} Дата выезда: {item.DepDate}");

            WriteLine("\n 6 и 8.	Вставка и удаление данных в таблицы, стоящей на стороне отношения «Один>\n");
            WriteLine("до добавления номеров");
            rooms = db.HotelRooms.ToList();
            foreach (HotelRoom room in rooms)
                WriteLine($"Тип: {room.Type}  Стоимость: {room.Coast} Вместимость: {room.Capacity} Описание: {room.Description}");
            WriteLine("после добавления Номеров");
            HotelRoom hotRoom = new HotelRoom() { Type = "Люкс",Coast =300, Capacity = 4, Description = "Самый большой номер с лучшим комфортом и огромной вместимостью" };
            db.HotelRooms.Add(hotRoom);
            db.SaveChanges();
            rooms = db.HotelRooms.ToList();
            foreach (HotelRoom room in rooms)
                WriteLine($"Тип: {room.Type}  Стоимость: {room.Coast}  Вместимость: {room.Capacity} Описание: {room.Description}");
            WriteLine("после удаления номера");
            db.HotelRooms.Remove(hotRoom);
            db.SaveChanges();
            rooms = db.HotelRooms.ToList();
            foreach (HotelRoom room in rooms)
                WriteLine($"Тип: {room.Type} Стоимость: {room.Coast} Вместимость: {room.Capacity} Описание: {room.Description}");


            WriteLine("\n 7 и 9.	Вставка и удаление данных в таблицы, стоящей на стороне отношения «Один>\n");
            WriteLine("до добавления номеров");
            var cust = db.Customers.ToList();
            foreach (Customer customer in cust)
                WriteLine($"Индефикатор: {customer.Id}  Индефикатор номера: {customer.IdRoom} ФИО: {customer.FIO} Паспорт: {customer.Passport}");
            WriteLine("после добавления Номеров");
            Customer f = new Customer() { IdRoom = 4, FIO = "Кузьмин Ф.Д", Passport = "HB2334859023" };
            db.Customers.Add(f);
            db.SaveChanges();
            cust = db.Customers.ToList();
            foreach (Customer customer in cust)
                WriteLine($"Индефикатор: {customer.Id}  Индефикатор номера: {customer.IdRoom} ФИО: {customer.FIO} Паспорт: {customer.Passport}");

            WriteLine("после удаления номера");
            db.Customers.Remove(f);
            db.SaveChanges();
            cust = db.Customers.ToList();
            foreach (Customer customer in cust )
                WriteLine($"Индефикатор: {customer.Id}  Индефикатор номера: {customer.IdRoom} ФИО: {customer.FIO} Паспорт: {customer.Passport}");

            WriteLine("\n10.	Обновление удовлетворяющих определенному условию записей в любой из таблиц базы данных\n");
            WriteLine("номера до обновления");
            var hotelRooms = db.HotelRooms.ToList();
            foreach (HotelRoom hotelRoom in hotelRooms)
                WriteLine($"Идентификатор: {hotelRoom.Id}  Тип: {hotelRoom.Type} Стоимость: {hotelRoom.Coast} Вместимость{hotelRoom.Capacity} Описание{hotelRoom.Description}" );
            hotelRooms = db.HotelRooms.Where(p => p.Coast == 300).ToList();

            foreach (HotelRoom hotelRoom in hotelRooms)
                hotelRoom.Coast = 120;
            db.SaveChanges();
            WriteLine("номера после обновления");
            hotelRooms = db.HotelRooms.ToList();
            foreach (HotelRoom hotelRoom in hotelRooms)
                WriteLine($"Идентификатор: {hotelRoom.Id}  Тип: {hotelRoom.Type} Стоимость: {hotelRoom.Coast} Вместимость{hotelRoom.Capacity} Описание{hotelRoom.Description}" );

            ReadKey();

        }
    }
}
